+++
date = 2021-01-13T12:00:33Z
draft = true
lastmod = 2021-01-13T12:00:33Z
tag = []
title = "Search API"
title_image = ""

+++
เพย์ไนน์แพลตฟอร์ม จะใช้ Elastic Search ทำฐานข้อมูลต่างๆ เช่น ข้อมูลสินค้า เพื่อความเร็วในการทำงาน และรองรับการขยายตัว จึงออกแบบให้มี Search API service คั่นไว้ เขียนด้วย go โค้ดอยู่ที่ [Gitlab](https://gitlab.com/pay9/search "Search Service API") 

**API หลักประกอบไปด้วย**

1. Import Bulk Data
2. Insert Item
3. Update Item
4. Delete Item
5. Search Item